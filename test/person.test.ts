import * as chai from "chai";
import "mocha";
import chaiHTTP = require("chai-http");

import app from "../src/app";

// valid test Person definitions
const testData = require("./data/testperson.json");
// invalid test Person definitions (missing attributes)
const falseData = require("./data/falseperson.json");

chai.use(chaiHTTP);
const expect = chai.expect;

// for keeping count of the number of Persons
let personCount: number;
// id value of the Person added in the tests
let testPersonID: number;
// index of the test data used
let testIndex = Math.floor((Math.random() * 4));
console.log(`  Using test set ${testIndex}`);

//GET list of Person
describe("The GET endpoint /person", () => {
    it("should respond with a JSON array", () => {
        return chai.request(app).get("/person")
            .then(res => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an("array");
                expect(res.body).to.have.length.greaterThan(0);

                personCount = res.body.length;
            });
    });

    it("should include Henk van der Steen", () => {
        return chai.request(app).get("/person")
            .then(res => {
                let henk = res.body.find(person => person.name.first === "Henk"
                                                && person.name.prefix === "van der"
                                                && person.name.last === "Steen");
                expect(henk).to.exist;
                expect(henk).to.contain.all.keys([
                    "id",
                    "name",
                    "phone",
                    "activity"
                ]);
                expect(henk.name).to.contain.all.keys([
                    "first",
                    "last"
                ])
            });
    });
});

//POST a new Person
describe("The POST endpoint /person", () => {
    it("should accept a valid Person definition", () => {
        return chai.request(app).post("/person")
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(201);
                expect(res.body).to.contain.keys(["id"]);
                testPersonID = res.body.id;
            });
    });

    it("should have added the given valid Person to the data set at the returned id", () => {
        return chai.request(app).get("/person")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = personCount;
                personCount = res.body.length;

                expect(res.body).to.have.length(prevCount + 1);

                let test = res.body.find(person => person.name.first  === testData[testIndex].name.first
                                                && person.name.prefix === testData[testIndex].name.prefix
                                                && person.name.last   === testData[testIndex].name.last);
                expect(test).to.exist;
                expect(test.id).to.equal(testPersonID);
            });
    });

    it("should NOT accept an invalid Person definition", () => {
        return chai.request(app).post("/person")
            .send(falseData[testIndex])
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT have added the given invalid Person to the data set", () => {
        return chai.request(app).get("/person")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = personCount;
                personCount = res.body.length;

                expect(res.body).to.have.length(prevCount);

                let test = res.body.find(person => person.name === testData[testIndex].name);
                expect(test).to.not.exist;
            });
    });
});

//GET Person by id
describe("The GET endpoint /person/:id", () => {
    it("should return a single JSON object", () => {
        return chai.request(app).get(`/person/${testPersonID}`)
            .then(res => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an("object");
            });
    });

    it("should return the testPerson's details", () => {
        return chai.request(app).get(`/person/${testPersonID}`)
            .then(res => {
                expect(res.body.name).to.deep.equal(testData[testIndex].name);
            });
    });

    it("should NOT accept non-existing ids", () => {
        return chai.request(app).get("/person/800")
            .then(res => {
                expect(res).to.have.status(404);
            });
    });
});

//PUT Person by id
describe("The PUT endpoint /person/:id", () => {
    before((done) => {
        testIndex++;
        testIndex %= 3;
        console.log(`    putting with test set ${testIndex}`);
        done();
    });

    it("should accept a valid redefinition of an existing Person", () => {
        return chai.request(app).put(`/person/${testPersonID}`)
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should not change the number of Persons in the data set", () => {
        return chai.request(app).get("/person")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = personCount;
                personCount = res.body.length;

                expect(res.body).to.have.length(prevCount);
            });
    });

    it("should change the data of the identified Person", () => {
        return chai.request(app).get(`/person/${testPersonID}`)
            .then(res => {
                expect(res.body.name).to.deep.equal(testData[testIndex].name);
                expect(res.body.phone).to.equal(testData[testIndex].phone);
                expect(res.body.activity).to.equal(testData[testIndex].activity);
            });
    });

    it("should be idempotent", () => {
        return chai.request(app).put(`/person/${testPersonID}`)
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(204);
                return chai.request(app).get(`/person/${testPersonID}`);
            }).then(res => {
                expect(res.body.name).to.deep.equal(testData[testIndex].name);
                expect(res.body.phone).to.equal(testData[testIndex].phone);
                expect(res.body.activity).to.equal(testData[testIndex].activity);
            });
    });

    it("should not accept non-existing ids", () => {
        return chai.request(app).put("/person/800")
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(404);
            });
    });

    it("should not accept an invalid redefinition", () => {
        return chai.request(app).put(`/person/${testPersonID}`)
            .send(falseData[testIndex])
            .then(res => {
                expect(res).to.have.status(400);
            });
    });
});

//TODO: PATCH person by id???

//DELETE Person by id
describe("The DELETE endpoint /person/:id", () => {
    it("should accept a Person's id", () => {
        return chai.request(app).del(`/person/${testPersonID}`)
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should have deleted the Person", () => {
        return chai.request(app).get("/person")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = personCount;
                personCount = res.body.length;

                expect(res.body).to.have.length(prevCount - 1);

                let test = res.body.find(person => person.name.first === testData[testIndex].name.first
                                                && person.name.last  === testData[testIndex].name.last);
                expect(test).to.not.exist;
            });
    });

    it("should also accept valid ids that do not exist (any more)", () => {
        return chai.request(app).del("/person/800")
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should not delete a Person in that case", () => {
        return chai.request(app).get("/person")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = personCount;
                personCount = res.body.length;

                expect(res.body).to.have.length(prevCount);
            });
    });
});