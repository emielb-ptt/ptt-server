import * as chai from "chai";
import "mocha";
import chaiHTTP = require("chai-http");

import app from "../src/app";

chai.use(chaiHTTP);
const expect = chai.expect;

describe("The base route", () => {
    it("should return content type JSON", () => {
        return chai.request(app).get("/")
            .then(res => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
            });
    });

    it("should return a set of properties", () => {
        return chai.request(app).get("/")
            .then(res => {
                expect(res.body.message).to.equal("Hello World!");
                expect(res.body.name).to.equal("ptt-server");
                expect(res.body.version).to.match(/[0-9]+\.[0-9]+\.[0-9]+/);
                expect(res.body.date).to.be.a("string");
            });
    });
});