import * as chai  from "chai";
import chaiHTTP = require("chai-http");
import "mocha";

import app          from "../src/app";
import { Activity } from "../src/types/activity";

// valid test Activity definitions
const testData = require("./data/testactivity.json");
// invalid test Activity definitions (missing attributes)
const falseData = require("./data/falseactivity.json");

chai.use(chaiHTTP);
const expect = chai.expect;

// for keeping count of the number of Activities
let actCount: number;
// index of the test data used
let testIndex: number;

describe("The GET endpoint /activity", () => {
    it("should respond with a JSON array", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an("array");
                expect(res.body).to.have.length.greaterThan(0);

                actCount = res.body.length;
            });
    });

    it("should include Delta", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                let delta = res.body.find(act => act.name === "Delta");
                expect(delta).to.exist;
                expect(delta).to.contain.all.keys([
                    "id",
                    "name"
                ]);
            });
    });
});

describe("The PUT endpoint /activity", () => {
    let al: any[];
    let rl: any[];

    before("uses a test item", () => {
        testIndex = Math.floor((Math.random() * testData.length));
        console.log(`    putting with test set ${testIndex}`);

        al = [
            {
                id: "A",
                name: "Antelope",
                url: "http://www.jotihunt.net/alpha.html"
            },
            {
                id: "B",
                name: "Bravo",
                url: "http://www.jotihunt.net/bravo.html"
            },
            {
                id: "C",
                name: "Charlie",
                url: "http://www.jotihunt.net/charlie.html"
            },
            {
                id: "D",
                name: "Delta",
                url: "http://www.jotihunt.net/delta.html"
            },
            {
                id: "E",
                name: "Echo",
                url: "http://www.jotihunt.net/echo.html"
            },
            {
                id: "F",
                name: "Foxtrot",
                url: "http://www.jotihunt.net/foxtrot.html"
            },
            {
                id: "H",
                name: "Hints",
                url: "http://www.jotihunt.net"
            },
            {
                id: "K",
                name: "Creative",
                url: "http://www.jotihunt.net"
            },
            {
                id: "L",
                name: "Away"
            },
            {
                id: "P",
                name: "Photos"
            },
            {
                id: "S",
                name: "Asleep",
                url: "https://en.wikipedia.org/wiki/Counting_sheep"
            },
            testData[testIndex]
        ];
    });

    it("should accept a valid redefinition of the Activities", () => {
        return chai.request(app).put("/activity")
            .send(al)
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should add new Activities", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                // to check idempotency later
                rl = res.body;

                expect(res.body).to.have.length(prevCount + 1);

                let test = res.body.find(act => act.id   === testData[testIndex].id
                                             && act.name === testData[testIndex].name
                                             && (testData[testIndex].url
                                                ? act.url === testData[testIndex].url
                                                : !act.url));
                expect(test).to.exist;
            });
    });

    it("should change Activities", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                let changedA = res.body.filter(act => act.id === "A")[0];
                expect(changedA).to.exist;
                expect(changedA.name).to.equal("Antelope");
            });
    });

    it("should be idempotent", () => {
        return chai.request(app).put("/activity")
            .send(al)
            .then(res => {
                expect(res).to.have.status(204);
                return chai.request(app).get("/activity");
            })
            .then(res => {
                expect(res.body).to.deep.equal(rl);
            });
    });

    it("should NOT accept a redefinition missing one of the Activities", () => {
        al[0].name = "Asterix";
        let al2 = al;
        al2.pop();

        return chai.request(app).put("/activity")
            .send(al2)// missing last element
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a redefinition with a double Activity id", () => {
        let al2 = al;
        al2.push(al2[0]);

        return chai.request(app).put("/activity")
            .send(al2)// contains two Activities with the first Activity's id
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a redefinition with an Activity that has no id", () => {
        let al2 = al;
        al2.push(falseData.no_id);

        return chai.request(app).put("/activity")
            .send(al2)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a redefinition with an Activity that has an empty id", () => {
        let al2 = al;
        al2.push(falseData.empty_id);

        return chai.request(app).put("/activity")
            .send(al2)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a redefinition with an Activity that has an invalid id", () => {
        let al2 = al;
        al2.push(falseData.invalid_id);

        return chai.request(app).put("/activity")
            .send(al2)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a redefinition with an Activity that has no name", () => {
        let al2 = al;
        al2.push(falseData.no_name);

        return chai.request(app).put("/activity")
            .send(al2)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT add or change Activities for an invalid request", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                expect(res.body).to.have.length(prevCount);

                let aAct = res.body.find(act => act.id === "A");
                expect(aAct).to.exist;
                expect(aAct.name).to.equal("Antelope");
            });
    });
});

describe("The GET endpoint /activity/:id", () => {
    it("should respond with an Activity object", () => {
        return chai.request(app).get(`/activity/${testData[testIndex].id}`)
            .then(res => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an("object");

                let act: Activity;
                try {
                    act = new Activity(res.body);
                } catch (err) {
                    expect(err).to.not.exist;
                }

                expect(act).to.exist;
            });
    });

    it("should return the test activity", () => {
        return chai.request(app).get(`/activity/${testData[testIndex].id}`)
            .then(res => {
                let act = new Activity(res.body);
                expect(act.id).to.equal(testData[testIndex].id);
                expect(act.name).to.equal(testData[testIndex].name);
                if (testData[testIndex].url) {
                    expect(act.url).to.exist;
                    expect(act.url).to.equal(testData[testIndex].url);
                } else {
                    expect(act.url).to.not.exist;
                }
            });
    });

    it("should NOT accept non-existing ids", () => {
        return chai.request(app).get("/activity/V")
            .then(res => {
                expect(res).to.have.status(404);
            });
    });
});

describe("The DELETE endpoint /activity/:id", () => {
    it("should accept an empty Activity's id", () => {
        return chai.request(app).del(`/activity/${testData[testIndex].id}`)
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should have deleted the Activity", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                expect(res.body).to.have.length(prevCount - 1);

                let test = res.body.find(act => act.id === testData[testIndex].id);
                expect(test).to.not.exist;
            });
    });

    it("should also accept ids that do not exist (any more)", () => {
        return chai.request(app).del(`/activity/Q`)
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should NOT accept a populated Activity's id", () => {
        return chai.request(app).del(`/activity/A`)
            .then(res => {
                expect(res).to.have.status(409);
            });
    });

    it("should NOT delete an Activity in both those cases", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                expect(res.body).to.have.length(prevCount);
            });
    });
});

describe("The PUT endpoint /activity/:id", () => {//update Swagger? ch descr, add URL
    before("uses the next test item", () => {
        testIndex = (testIndex + 1) % testData.length;
        console.log(`    putting with test set ${testIndex}`);
    });

    it("should accept a valid redefinition of an existing Activity", () => {
        return chai.request(app).put("/activity/B")
            .send({
                id: "B",
                name: "Ben"
            })
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should NOT change the number of Activities in the data set on redefinition", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                expect(res.body).to.have.length(prevCount);
            });
    });

    it("should accept a valid definition of a new Activity", () => {
        return chai.request(app).put(`/activity/${testData[testIndex].id}`)
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should add an Activity to the data set for a new definition", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                // make sure that, in case this test fails, the correct number is kept
                let prevCount = actCount;
                actCount = res.body.length;

                expect(res.body).to.have.length(prevCount + 1);
            });
    });

    it("should persist the data of the identified Activity", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                let testChange = res.body.find(act => act.id === "B");

                expect(testChange).to.exist;
                expect(testChange.name).to.equal("Ben");
                expect(testChange.url).to.not.exist;

                let testAdd = res.body.find(act => act.id === testData[testIndex].id);

                expect(testAdd).to.exist;
                expect(testAdd.name).to.equal(testData[testIndex].name);
                if (testData[testIndex].url) {
                    expect(testAdd.url).to.exist;
                    expect(testAdd.url).to.equal(testData[testIndex].url);
                } else {
                    expect(testAdd.url).to.not.exist;
                }
            });
    });

    it("should be idempotent", () => {
        return chai.request(app).put(`/activity/${testData[testIndex].id}`)
            .send(testData[testIndex])
            .then(res => {
                expect(res).to.have.status(204);
                return chai.request(app).get("/activity")
                .then(res => {
                    let test = res.body.find(act => act.id === testData[testIndex].id);

                    expect(test).to.exist;
                    expect(test.name).to.equal(testData[testIndex].name);
                    if (testData[testIndex].url) {
                        expect(test.url).to.exist;
                        expect(test.url).to.equal(testData[testIndex].url);
                    } else {
                        expect(test.url).to.not.exist;
                    }
                });
            });
    });

    it("should NOT accept a definition without an id", () => {
        return chai.request(app).put("/activity/B")
            .send(falseData.no_id)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a definition with an invalid id", () => {
        return chai.request(app).put(`/activity/${falseData.invalid_id.id}`)
            .send(falseData.invalid_id)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a definition with an inconsistent id", () => {
        let inconsistent = testData[(testIndex + 1) % testData.length];
        inconsistent.id = "Q";

        return chai.request(app).put("/activity/B")
            .send(inconsistent)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a definition without a name", () => {
        return chai.request(app).put(`/activity/${falseData.no_name.id}`)
            .send(falseData.no_name)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT accept a definition for an invalid id", () => {
        return chai.request(app).put(`/activity/${falseData.invalid_id.id}`)
            .send(falseData.invalid_id)
            .then(res => {
                expect(res).to.have.status(400);
            });
    });

    it("should NOT change the Activity for an invalid request", () => {
        return chai.request(app).get("/activity")
            .then(res => {
                let test = res.body.find(act => act.id   === "B"
                                             && act.name === "Ben"
                                             && !act.url);

                expect(test).to.exist;
            })
    })
});

describe("The GET endpoint /activity/:id/person", () => {
    it("should respond with a JSON array", () => {
        return chai.request(app).get("/activity/B/person")
            .then(res => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an("array");
            });
    });

    it("should include Henk van der Steen", () => {
        return chai.request(app).get("/activity/B/person")
            .then(res => {
                let henk = res.body.find(p => p.name
                                           && p.name.first  === "Henk"
                                           && p.name.prefix === "van der"
                                           && p.name.last   === "Steen");

                expect(henk).to.exist;
            });
    });

    it("should NOT accept non-existing ids", () => {
        return chai.request(app).get("/activity/V/person")
            .then(res => {
                expect(res).to.have.status(404);
            });
    });
});

describe("The GET endpoint /activity/:id/url", () => {
    it("should return a redirect to the right URL", () => {
        return chai.request(app).get("/activity/C/url")
            .then(res => {
                expect(res).to.redirect;
                expect(res["redirects"][0]).to.equal("http://www.jotihunt.net/charlie.html");
            });
    });

    it("should return no content if the Activity has no URL", () => {
        return chai.request(app).get("/activity/L/url")
            .then(res => {
                expect(res).to.have.status(204);
            });
    });

    it("should NOT accept non-existing ids", () => {
        return chai.request(app).get("/activity/V/url")
            .then(res => {
                expect(res).to.have.status(404);
            });
    });
});
