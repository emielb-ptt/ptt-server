'use strict';

var url = require('url');

var Person = require('./PersonService');

module.exports.personGET = function personGET (req, res, next) {
  Person.personGET(req.swagger.params, res, next);
};

module.exports.personIdDELETE = function personIdDELETE (req, res, next) {
  Person.personIdDELETE(req.swagger.params, res, next);
};

module.exports.personIdGET = function personIdGET (req, res, next) {
  Person.personIdGET(req.swagger.params, res, next);
};

module.exports.personIdPUT = function personIdPUT (req, res, next) {
  Person.personIdPUT(req.swagger.params, res, next);
};

module.exports.personPOST = function personPOST (req, res, next) {
  Person.personPOST(req.swagger.params, res, next);
};
