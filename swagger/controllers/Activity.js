'use strict';

var url = require('url');

var Activity = require('./ActivityService');

module.exports.activityGET = function activityGET (req, res, next) {
  Activity.activityGET(req.swagger.params, res, next);
};

module.exports.activityIdDELETE = function activityIdDELETE (req, res, next) {
  Activity.activityIdDELETE(req.swagger.params, res, next);
};

module.exports.activityIdGET = function activityIdGET (req, res, next) {
  Activity.activityIdGET(req.swagger.params, res, next);
};

module.exports.activityIdPUT = function activityIdPUT (req, res, next) {
  Activity.activityIdPUT(req.swagger.params, res, next);
};

module.exports.activityIdPersonGET = function activityIdPersonGET (req, res, next) {
  Activity.activityIdPersonGET(req.swagger.params, res, next);
};

module.exports.activityIdUrlGET = function activityIdUrlGET (req, res, next) {
  Activity.activityIdUrlGET(req.swagger.params, res, next);
};

module.exports.activityPUT = function activityPUT (req, res, next) {
  Activity.activityPUT(req.swagger.params, res, next);
};
