'use strict';

exports.rootGET = function(args, res, next) {
  /**
   * Server info
   * The root get endpoint returns a status message with information on the server. 
   *
   * returns inline_response_200
   **/
  var examples = {};
  examples['application/json'] = {
  "date" : "2017-03-08T07:53:28.814Z",
  "name" : "ptt-server",
  "message" : "Hello World!",
  "version" : "0.1.0"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

