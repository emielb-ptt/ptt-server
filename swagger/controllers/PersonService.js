'use strict';

exports.personGET = function(args, res, next) {
  /**
   * All persons
   * The person get endpoint returns a list of all the persons in the system. 
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "activity" : "Alfa",
  "phone" : "0612345678",
  "name" : {
    "last" : "Gunter",
    "prefix" : "tot",
    "first" : "Vanhir"
  },
  "id" : 6
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.personIdDELETE = function(args, res, next) {
  /**
   * Delete person
   * Delete a person, identified by the supplied person identifier as path parameter in the URL. 
   *
   * id Integer 
   * no response value expected for this operation
   **/
  res.end();
}

exports.personIdGET = function(args, res, next) {
  /**
   * One person
   * Get all details for one person, identified by the supplied person identifier as path parameter in the URL. 
   *
   * id Integer 
   * returns inline_response_200_1
   **/
  var examples = {};
  examples['application/json'] = {
  "activity" : "Alfa",
  "phone" : "0612345678",
  "name" : {
    "last" : "Gunter",
    "prefix" : "tot",
    "first" : "Vanhir"
  },
  "id" : 6
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.personIdPUT = function(args, res, next) {
  /**
   * Update person
   * Provide a new definition of a person, identified by the supplied person identifier as path parameter in the URL. Provided that the definition is valid, it will replace the current person definition. 
   *
   * id Integer 
   * person Person_1 
   * no response value expected for this operation
   **/
  res.end();
}

exports.personPOST = function(args, res, next) {
  /**
   * New person
   * The person post endpoint allows to post a new person, which will be entered into the system, provided that the definition is valid. 
   *
   * person Person 
   * returns inline_response_201
   **/
  var examples = {};
  examples['application/json'] = {
  "id" : 6,
  "message" : "Successfully created new person with id 6."
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

