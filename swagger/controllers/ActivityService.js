'use strict';

exports.activityGET = function(args, res, next) {
  /**
   * All activities
   * The activity get endpoint returns a list of all activities in the system. 
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "name" : "Asleep",
  "id" : "S",
  "url" : "http://www.example.com/"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.activityIdDELETE = function(args, res, next) {
  /**
   * Delete an activity
   * Delete an activity, identified by the supplied activity identifier as path parameter in the URL. 
   *
   * id String 
   * no response value expected for this operation
   **/
  res.end();
}

exports.activityIdGET = function(args, res, next) {
  /**
   * A single activity
   * Return the activity identified by the supplied activity identifier as path parameter in the URL. 
   *
   * id String 
   * returns inline_response_200_2
   **/
  var examples = {};
  examples['application/json'] = {
  "name" : "Asleep",
  "id" : "S",
  "url" : "http://www.example.com/"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.activityIdPUT = function(args, res, next) {
  /**
   * Change an activity
   * Change or add an activity, identified by the supplied activity identifier as path parameter in the URL. The definition will replace the current one. 
   *
   * id String 
   * activity Activity 
   * no response value expected for this operation
   **/
  res.end();
}

exports.activityIdPersonGET = function(args, res, next) {
  /**
   * All persons in the activity
   * Get all persons for one activity, identified by the supplied activity identifier as path parameter in the URL. 
   *
   * id String 
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "activity" : "Alfa",
  "phone" : "0612345678",
  "name" : {
    "last" : "Gunter",
    "prefix" : "tot",
    "first" : "Vanhir"
  },
  "id" : 6
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.activityIdUrlGET = function(args, res, next) {
  /**
   * Coupled URL
   * Get the URL that is coupled to an activity, identified by the supplied activity identifier as path parameter in the URL. 
   *
   * id String 
   * no response value expected for this operation
   **/
  res.end();
}

exports.activityPUT = function(args, res, next) {
  /**
   * Change all activities
   * Change all activities. All activities will be updated, provided that the list is complete and valid, *i.e.*  * no double ids * no missing ids * new ids are allowed, iff a unique one-character string * all activities have a string `name` * activities can have a string `url`  A list of the required activity ids can be obtained from the /activity get endpoint. 
   *
   * activities List 
   * no response value expected for this operation
   **/
  res.end();
}

