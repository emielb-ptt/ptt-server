import { Router } from "express";

import { PTTStorage } from "../storage/pttstorage";

export abstract class PTTRouter {
    protected static kind = "item";

    /**
     * Form a 400 - Bad Request response body.
     * @param {string} message the message in the response body
     * @return {{message: string, status: number}} the response body
     */
    protected static badRequest(message: string): { message: string, status: number } {
        return PTTRouter.statusMessage(400, message);
    }

    /**
     * Form a 404 - Not Found response body, based on the 'kind' static member.
     * @returns {{message: string, status: number}} the response body
     */
    protected static notFound(): { message: string, status: number } {
        return PTTRouter.statusMessage(404, `No ${this.kind} found with the given id.`);
    };

    /**
     * Form an HTTP status response body.
     * @param {number} status the status code of the response
     * @param {string} message the message in the response body
     * @returns {{message: string, status: number}} the response body
     */
    private static statusMessage(status: number, message: string): { message: string, status: number } {
        return { message: message, status: status};
    }

    router: Router;

    /**
     * Initialise the Express Router.
     */
    constructor() {
        this.router = Router();
    }

    /**
     * Check whether the PTTStorage is ready.
     * @param {PTTStorage} storage the PTTStorage to check
     */
    checkPTTStorage (storage: PTTStorage) {
        storage.isSetUp().subscribe({
            next: (done) => {
                if (!done) {
                    storage.setup();
                }
                this.init();
            }
        });
    }

    /**
     * Attach the handlers to the Express.Router's endpoints.
     */
    abstract init(): void;
}
