import { Request,
         Response } from "express";

import { MDBActivity     } from "../storage/mariadb/mariadb-activitystorage";
import { ActivityStorage } from "../storage/activitystorage";
import { Activity,
         ActivityError   } from "../types/activity";

import { PTTRouter } from "./pttrouter";
import {Person, PersonError} from "../types/person";

export class ActivityRouter extends PTTRouter {
    protected static kind = "Activity";

    static storage: ActivityStorage;

    /**
     * Initialise the ActivityRouter with storage.
     */
    constructor() {
        super();
        ActivityRouter.storage = new MDBActivity();
        this.checkPTTStorage(ActivityRouter.storage);
    }

    init(): void {
        this.router.get("/", this.getAll);
        this.router.put("/", this.putAll);
        this.router.get("/:id", this.getOne);
        this.router.put("/:id", this.putOne);
        this.router.delete("/:id", this.deleteOne);
        this.router.get("/:id/person", this.getPersons);
        this.router.get("/:id/url", this.getURL);
    }

    /**
     * GET all Activities.
     * @param req the received Request
     * @param res the Response to send
     */
    public getAll(req: Request, res: Response): void {
        let activities: Activity[] = [];
        ActivityRouter.storage.readAll()
            .subscribe({
                next: (a: Activity) => activities.push(a),
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(500).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                },
                complete: () => res.status(200).send(activities)
            });
    }

    /**
     * PUT all Activities.
     * @param req the received Request, with the data for all Activities as body
     * @param res the Response to send
     */
    public putAll(req: Request, res: Response): void {
        let activities: Activity[] = [];
        let errors: string[] = [];

        try {
            req.body.forEach((item, index) => {
                try {
                    activities.push(new Activity(item));
                } catch (err) {
                    if (err instanceof ActivityError) {
                        errors.push(`item ${index}: ${err.message}`);
                    } else {
                        throw err;
                    }
                }
            });
        } catch (err) {
            res.sendStatus(500);
            return;
        }

        if (errors.length > 0) {
            res.status(400).send(ActivityRouter.badRequest(errors.join("\n")));
            return;
        }

        ActivityRouter.storage.setAll(activities)
            .subscribe({
                next: (successful) => {
                    if (successful) {
                        res.status(204).end();
                    } else {
                        res.sendStatus(500);
                    }
                },
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(400).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                }
            });
    }

    /**
     * GET an Activity.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public getOne(req: Request, res: Response): void {
        let id = req.params.id;

        ActivityRouter.storage.readOne(id)
            .subscribe({
                next: (a: Activity) => {
                    if (a) {
                        res.status(200).send(a);
                    } else {
                        res.status(404).send(ActivityRouter.notFound());
                    }
                },
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(500).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                }
            });
    }

    /**
     * PUT an Activity.
     * @param req the received Request, with the id as path parameter and the Activity as body
     * @param res the Response to send
     */
    public putOne(req: Request, res: Response): void {
        let id = req.params.id;
        let activity: Activity;

        try {
            activity = new Activity(req.body);
        } catch (err) {
            if (err instanceof ActivityError) {
                res.status(400).send(ActivityRouter.badRequest(err.message));
            } else {
                res.sendStatus(500);
            }
            return;
        }

        ActivityRouter.storage.setOne(id, activity)
            .subscribe({
                next: (successful) => {
                    if (successful) {
                        res.status(204).end();
                    } else {
                        res.sendStatus(500);
                    }
                },
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(400).send(ActivityRouter.badRequest(err.message));
                    } else {
                        res.sendStatus(500);
                    }
                }
            });
    }

    /**
     * DELETE an Activity.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public deleteOne(req: Request, res: Response): void {
        let id = req.params.id;

        ActivityRouter.storage.deleteOne(id).subscribe({
            next: successful => {
                if (successful) {
                    res.status(204).end();
                } else {
                    res.status(409).send(new ActivityError(`Activity '${id}' cannot be removed, it is not empty.`));
                }
            },
            error: err => res.sendStatus(500)
        });
    }

    /**
     * GET all Persons in an Activity.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public getPersons(req: Request, res: Response): void {
        let id = req.params.id;
        let people: Person[] = [];

        ActivityRouter.storage.readPersons(id)
            .subscribe({
                next: (p: Person) => people.push(p),
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(404).send(err);
                    } else if (err instanceof PersonError) {
                        res.status(500).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                },
                complete: () => res.status(200).send(people)
            });
    }

    /**
     * GET the URL of an Activity.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public getURL(req: Request, res: Response): void {
        let id = req.params.id;

        ActivityRouter.storage.readURL(id)
            .subscribe({
                next: url => {
                    if (url) {
                        res.redirect(303, url);
                    } else {
                        res.status(204).end();
                    }
                },
                error: err => {
                    if (err instanceof ActivityError) {
                        res.status(404).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                }
            });
    }
}

// Create the ActivityRouter, and export its configured Express.Router
const activityRouter = new ActivityRouter();

export default activityRouter.router;
