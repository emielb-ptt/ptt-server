import { Request,
         Response } from "express";

import { MDBPerson     } from "../storage/mariadb/mariadb-personstorage";
import { PersonStorage } from "../storage/personstorage";
import { Person,
         PersonError   } from "../types/person";

import { PTTRouter } from "./pttrouter";

export class PersonRouter extends PTTRouter {
    protected static kind = "Person";

    static storage: PersonStorage;

    /**
     * Initialise the PersonRouter with storage.
     */
    constructor() {
        super();
        PersonRouter.storage = new MDBPerson();
        this.checkPTTStorage(PersonRouter.storage);
    }

    init(): void {
        this.router.get("/", this.getAll);
        this.router.post("/", this.postOne);
        this.router.get("/:id", this.getOne);
        this.router.put("/:id", this.putOne);
        this.router.delete("/:id", this.deleteOne);
    }

    /**
     * GET all Persons.
     * @param req the received Request
     * @param res the Response to send
     */
    public getAll(req: Request, res: Response): void {
        let persons: Person[] = [];
        PersonRouter.storage.readAll()
            .subscribe({
                next: (p: Person) => persons.push(p),
                error: err => {
                    if (err instanceof PersonError) {
                        res.status(500).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                },
                complete: () => res.status(200).send(persons)
            });
    }

    /**
     * POST a new Person.
     * @param req the received Request, with the data for the new Person as body
     * @param res the Response to send
     */
    public postOne(req: Request, res: Response): void {
        let person: Person;

        try {
            person = new Person(req.body);
        } catch (err) {
            if (err instanceof PersonError) {
                res.status(400)
                    .send(PersonRouter.badRequest(err.message));
            } else {
                res.sendStatus(500);
            }
            return;
        }

        PersonRouter.storage.createOne(person)
            .subscribe({
                next: (index: number) => {
                    console.log(index);
                    res.status(201).location(`/person/${index}`)
                        .send({
                            message: `Successfully created new person with id ${index}.`,
                            id: index
                        });
                },
                error: err => res.sendStatus(500)
            });
    }

    /**
     * GET one person by id.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public getOne(req: Request, res: Response): void {
        let query: number;

        try {
            query = parseInt(req.params.id);
        } catch (err) {
            res.status(400).send(PersonRouter.badRequest(`Not an ID: '${req.params.id}'`));
            return;
        }

        PersonRouter.storage.readOne(query)
            .subscribe({
                next: (person: Person) => {
                    if (person) {
                        res.status(200)
                            .send(person);
                    } else {
                        res.status(404)
                            .send(PersonRouter.notFound());
                    }
                },
                error: err => {
                    if (err instanceof PersonError) {
                        res.status(500).send(err);
                    } else {
                        res.sendStatus(500);
                    }
                }
            });
    }

    /**
     * PUT one person by id.
     * @param req the received Request, with the id as path parameter and the redefinition as body
     * @param res the Response to send
     */
    public putOne(req: Request, res: Response): void {
        let query: number;
        let person: Person;

        try {
            query = parseInt(req.params.id);
        } catch (err) {
            res.status(400).send(PersonRouter.badRequest(`Not an ID: '${req.params.id}'`));
            return;
        }

        try {
            person = new Person(req.body);
        } catch (err) {
            if (err instanceof PersonError) {
                res.status(400)
                    .send(PersonRouter.badRequest(err.message));
            } else {
                res.sendStatus(500);
            }
            return;
        }

        PersonRouter.storage.updateOne(query, person)
            .subscribe({
                next: (successful: boolean) => {
                    if (successful) {
                        res.status(204).end();
                    } else {
                        res.status(404)
                            .send(PersonRouter.notFound());
                    }
                },
                error: err => res.sendStatus(500)
            });
    }

    //patchOne?

    /**
     * DELETE a person by id.
     * @param req the received Request, with the id as path parameter
     * @param res the Response to send
     */
    public deleteOne(req: Request, res: Response): void {
        let query: number;

        try {
            query = parseInt(req.params.id);
        } catch (err) {
            res.status(400).send(PersonRouter.badRequest(`Not an ID: '${req.params.id}'`));
            return;
        }

        PersonRouter.storage.deleteOne(query)
            .subscribe({
                next: () => res.status(204).end(),
                error: err => res.sendStatus(500)
            });
    }
}

// Create the PersonRouter, and export its configured Express.Router
const personRouter = new PersonRouter();

export default personRouter.router;
