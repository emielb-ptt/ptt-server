import * as http  from "http";

import App       from "./app";
import { Level } from "./log/level";
import Logger    from "./log/logger";

Logger.setLogLevel(Level.debug);

Logger.info("Starting JOTI-PTT server...");

App.set("logger", Logger);
const port = normalisePort(process.env.PORT || 3000);
App.set("port", port);

const server = http.createServer(App)
    .listen(port)
    .on("error", onError)
    .on("listening", onListening);

function normalisePort(val: number|string): number|string|boolean {
    let port: number = (typeof val === "string") ? parseInt(val, 10) : val;
    if (isNaN(port)) {
        return val;
    } else if (port >= 0) {
        return port;
    } else {
        return false;
    }
}

function onError(error: NodeJS.ErrnoException): void {
    if (error.syscall !== "listen") {
        throw error;
    }

    let bind = (typeof port === "string") ? "Pipe " + port : "Port " + port;

    switch(error.code) {
        case "EACCES":
            Logger.panic(`${bind} requires elevated privileges`);
            break;
        case "EADDRINUSE":
            Logger.panic(`${bind} is already in use`);
            break;
        default:
            throw error;
    }
}

function onListening(): void {
    let addr = server.address();
    let bind = (typeof addr == "string") ? `pipe ${addr}` : `port ${addr.port}`;
    Logger.info(`Now listening on ${bind}`);
}
