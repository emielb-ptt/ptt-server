import * as express    from "express";
import * as logger     from "morgan";
import * as bodyParser from "body-parser";

import PersonRouter   from "./routes/personrouter";
import ActivityRouter from "./routes/activityrouter";

// Creates and configures an ExpressJS web server.
class App {
    // ref to the Express instance
    public express: express.Application;

    // Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(logger("dev"));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    // Configure API endpoints.
    private routes(): void {
        let router = express.Router();
        router.get("/", (req, res) => {
            res.json({
                message: "Hello World!",
                name:    process.env.npm_package_name,
                version: process.env.npm_package_version,
                date:    new Date().toISOString()
            });
        });

        this.express.use("/", router);
        this.express.use("/person", PersonRouter);
        this.express.use("/activity", ActivityRouter);
    }
}

export default new App().express