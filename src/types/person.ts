import { Activity } from "./activity";

export class Person {
    id?: number;
    name: {
        first:   string,
        prefix?: string,
        last:    string
    };
    phone: string;
    activity: Activity;

    constructor(info: any) {
        if (!info) {
            throw new PersonError(`Not a person definition: ${JSON.stringify(info)}`);
        } else if (!info.name || !info.name.first || !info.name.last || !info.phone || !info.activity) {
            let missing: string[] = [];

            if (!info.name) {
                missing.push("name {}");
            } else {
                if (!info.name.first) {
                    missing.push("first name");
                }
                if (!info.name.last) {
                    missing.push("surname");
                }
            }

            if (!info.phone) {
                missing.push("phone number");
            }

            if (!info.activity) {
                missing.push("activity");
            }

            throw new PersonError(`Person definition lacks the following properties: ${missing.join(", ")}`);
        }

        this.id = info.id;
        this.name = {
            first: info.name.first,
            prefix: info.name.prefix,
            last: info.name.last
        };
        this.phone = info.phone;
        this.activity = info.activity;
    }

    public static fromDatabaseRecord(record: any): Person {
        let person = new Person({
            id: record.id,
            name: {
                first: record.first_name,
                last: record.last_name
            },
            phone: record.phone,
            activity: record.activity
        });

        if (record.name_prefix) {
            person.name.prefix = record.name_prefix;
        }

        return person;
    }
}

export class PersonError extends TypeError {}