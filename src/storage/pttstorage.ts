import { Observable } from "rxjs";

export interface PTTStorage {
    /**
     * Prepare the PTTStorage to be used.
     */
    setup(): void;

    /**
     * Check whether the PTTStorage is set up.
     * @returns {Observable<boolean>} emits whether the PTTStorage is set up
     */
    isSetUp(): Observable<boolean>;
}