import { Observable } from "rxjs";

import { Person } from "../types/person";

import { PTTStorage } from "./pttstorage";

/**
 * The PersonStorage interface abstracts different ways of (persistently) storing and retrieving Person details.
 */
export interface PersonStorage extends PTTStorage {
    /**
     * Create a new person in the PersonStorage.
     * @param person the Person to be added
     * @returns {Observable<number>} emits the new Person's id
     */
    createOne(person: Person): Observable<number>;

    /**
     * Read all details of one Person from PersonStorage.
     * @param id the id of the Person
     * @returns {Observable<Person>} emits the Person
     */
    readOne(id: number): Observable<Person>;

    /**
     * Read all Persons from PersonStorage.
     * @returns {Observable<Person>} emits each Person, completes after all Persons are emitted
     */
    readAll(): Observable<Person>;

    /**
     * Update the details of a Person in PersonStorage.
     * @param id the id of the Person
     * @param person the new details of the Person
     * @returns {Observable<boolean>} emits the success of the operation
     */
    updateOne(id: number, person: Person): Observable<boolean>;

    /**
     * Delete a Person from PersonStorage.
     * @param id the id of the Person
     * @returns {Observable<boolean>} emits the success of the operation
     */
    deleteOne(id: number): Observable<boolean>;
}