import { Observable } from "rxjs";

import { Activity } from "../types/activity";
import { Person   } from "../types/person";

import { PTTStorage } from "./pttstorage";

/**
 * The ActivityStorage interface abstracts different ways of (persistently) storing and retrieving Activity details.
 */
export interface ActivityStorage extends PTTStorage {
    /**
     * Read all Activities from ActivityStorage.
     * @returns {Observable<Activity>} emits each Activity, completes after all Activities are emitted
     */
    readAll(): Observable<Activity>;

    /**
     * Set the details of all Activities in ActivityStorage.
     * New Activities will be added. All existing Activities must be present.
     * @param {Activity[]} activities the new list of all Activities
     * @returns {Observable<boolean>} emits the success of the operation
     */
    setAll(activities: Activity[]): Observable<boolean>;

    /**
     * Read one Activity from ActivityStorage.
     * @param {string} id the id of the Activity
     * @returns {Observable<Activity>} emits the Activity
     */
    readOne(id: string): Observable<Activity>;

    /**
     * Set the details of an Activity in ActivityStorage.
     * A new Activity will be added.
     * @param {string} id the id of the Activity
     * @param {Activity} activity the new details of the Activity
     * @returns {Observable<boolean>} emits the success of the operation
     */
    setOne(id: string, activity: Activity): Observable<boolean>;

    /**
     * Delete an Activity from ActivityStorage.
     * @param {string} id the id of the Activity
     * @returns {Observable<boolean>} emits the success of the operation
     */
    deleteOne(id: string): Observable<boolean>;

    /**
     * Read all Persons for an Activity from PTTStorage.
     * @param {string} id the id of the Activity
     * @returns {Observable<Person>} emits every Person in the Activity, then completes
     */
    readPersons(id: string): Observable<Person>;

    /**
     * Read the URL for an Activity from ActivityStorage.
     * @param {string} id the id of the Activity
     * @returns {Observable<string>} emits the URL or an empty string if absent
     */
    readURL(id: string): Observable<string>;
}
