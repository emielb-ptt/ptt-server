export class StorageError extends Error {
    constructor(msg?: string) {
        super();

        if (!msg) {
            this.message = "An unknown error occured while trying to access the PTTStorage.";
        } else {
            this.message = `An error occured while trying to access the Storage: ${msg}`;
        }
    }
}
