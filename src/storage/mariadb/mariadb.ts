import Client = require('mariasql');
import { Observable } from "rxjs";

import Logger from "../../log/logger";

import { StorageError } from "../storageerror";

export class MariaDBStorage {
    private connected: boolean;
    private database;

    constructor() {
        this.connected = false;
    }

    /**
     * Connect to the MariaDB server.
     */
    setup(): void {
        this.database = new Client({
            host: "localhost",
            user: "ptt",
            password: "mariadb",
            db: "ptt_db"
        });

        this.database.rxQuery = Observable.bindNodeCallback(this.database.query.bind(this.database));
        this.connected = true;
    }

    /**
     * @return {Observable<boolean>} emits whether or not there is a valid server connection.
     */
    isSetUp(): Observable<boolean> {
        return Observable.of(this.connected);
    }

    /**
     * Send a query to the MariaDB server and return the answer. Also log the query text.
     * @param query the query text
     * @return {Observable<any>} emits the results of the query, per row
     */
    query(query: string): Observable<any> {
        return this.readyCheck()
            .flatMap(() => {
                Logger.debug(`Sent query to MariaDB:\n${query}`);
                return this.database.rxQuery(query);
            });
    }

    /**
     * Check the connection with the server's MariaDB storage.
     * @return {Observable<true>} emits true when the server has a connection to the database, or throws a StorageError
     */
    private readyCheck(): Observable<boolean> {
        if (!this.connected) {
            return Observable.throw(new StorageError("No MariaDB connection (yet)..."));
        } else {
            return this.database.rxQuery("SELECT CONNECTION_ID();")
                .map(x => true)
                .retryWhen((errs) => {
                    return errs.delay(3000)
                        .scan((errorCount, err) => {
                            if(errorCount >= 2) {
                                throw err;
                            }

                            errorCount += 1;
                            console.log(`Could not connect to MariaDB. Retry no. ${errorCount} in 3 seconds...\n` + err);
                            return errorCount;
                        }, 0);
                })
                .catch(err => {
                    console.error("LOST MariaDB CONNECTION. RECONNECTING...\n" + err);
                    this.connected = false;
                    this.setup();
                    Observable.throw(new StorageError("Lost MariaDB connection. Reconnecting..."))
                });
        }
    }
}

// Create a MariaDBStorage, and export it
const mdbs = new MariaDBStorage();

export default mdbs;