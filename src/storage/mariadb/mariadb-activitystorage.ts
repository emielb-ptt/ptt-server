import { Observable } from "rxjs";

import { Activity,
         ActivityError } from "../../types/activity";
import { Person        } from "../../types/person";

import { ActivityStorage } from "../activitystorage";
import { MDBStorage      } from "./mariadb-storage";
import MDBStore            from "./mariadb";

export class MDBActivity extends MDBStorage implements ActivityStorage {
    readAll(): Observable<Activity> {
        let queryString =
            "SELECT id,\n" +
            "       name,\n" +
            "       url\n" +
            "    FROM Activities;";

        return MDBStore.query(queryString)
            .flatMap(x => Observable.from(x))
            .map(act => act ? new Activity(act) : null);
    }

    setAll(activities: Activity[]): Observable<boolean> {
        return this.readAll()
            .reduce((acc, curr) => {
                if (!activities.some(act => act.id === curr.id)) {
                    throw new ActivityError("Missing Activity");
                }
                return acc;
            })
            .flatMap(() => {
                try {
                    return this.setActivities(...activities);
                } catch (err) {
                    return Observable.throw(err);
                }
            });
    }

    readOne(id: string): Observable<Activity> {
        let queryString =
            "SELECT id,\n" +
            "       name,\n" +
            "       url\n" +
            "    FROM Activities\n" +
            `    WHERE id='${id}';`;

        return MDBStore.query(queryString)
            .map(array => array[0] ? array[0] : null)
            .map(act => act ? new Activity(act) : null);
    }

    setOne(id: string, activity: Activity): Observable<boolean> {
        if (id !== activity.id) {
            return Observable.throw(new ActivityError(`ID mismatch. URL: ${id}, item: ${activity.id}`));
        }

        try {
            return this.setActivities(activity);
        } catch (err) {
            return Observable.throw(err);
        }
    }

    deleteOne(id: string): Observable<boolean> {
        let queryString =
            "DELETE FROM Activities\n" +
            `    WHERE id='${id}';`;

        return MDBStore.query(queryString)
            .map(() => true)
            .catch(err => {
                if (err.code === 1451) {
                    return Observable.of(false);
                } else {
                    throw err;
                }
            });
    }

    readPersons(id: string): Observable<Person> {
        return this.checkActivityPresence(id, () => {
            let queryString =
                "SELECT Persons.id,\n" +
                "       Persons.first_name,\n" +
                "       Persons.name_prefix,\n" +
                "       Persons.last_name,\n" +
                "       Persons.phone,\n" +
                "       Activities.name AS activity\n" +
                "    FROM Persons LEFT JOIN Activities\n" +
                "    ON Persons.activity=Activities.id\n" +
                `    WHERE Activities.id='${id}';`;

            return MDBStore.query(queryString)
                .flatMap((x) => Observable.from(x))
                .map(dbo => dbo ? Person.fromDatabaseRecord(dbo) : null);
        });
    }

    readURL(id: string): Observable<string> {
        return this.checkActivityPresence(id, () => {
            let queryString =
                "SELECT url\n" +
                "    FROM Activities\n" +
                `    WHERE id='${id}';`;

            return MDBStore.query(queryString)
                .flatMap(x => Observable.from(x))
                .map((act: any) => act ? act.url : null);
        });
    }

    /**
     * Check whether an Activity is present in the database.
     * Then do something with it if it is, or give an ActivityError otherwise.
     * @param {string} id the id of the Activity
     * @param {() => Observable<any>} process what to do with the Activity
     * @returns {Observable<any>} emits the result of whatever is done with the Activity
     */
    private checkActivityPresence(id: string, process: () => Observable<any>): Observable<any> {
        return this.readAll()
            .first(a => a.id === id, () => true, false)
            .flatMap(found => {
                if (!found) {
                    return Observable.throw(new ActivityError(`Activity with id ${id} does not exist.`));
                }

                return process();
            });
    }

    /**
     * Insert Activities into the Activities table, or update them if they already exist.
     * @param {...Activity} activities varargs Activities to set
     * @throws {ActivityError} if an invalid ID is encountered
     * @returns {Observable<boolean>} emits true on success, or errors
     */
    private setActivities(...activities: Activity[]): Observable<boolean> {
        let head =
            "INSERT INTO Activities (\n" +
            "        id,\n" +
            "        name,\n" +
            "        url\n" +
            "    )\n" +
            "    VALUES\n";

        let tail =
            "    ON DUPLICATE KEY UPDATE\n" +
            "        name=VALUES(name),\n" +
            "        url=VALUES(url);";

        let values = activities.reduce((v, act) => {
            Activity.checkID(act.id);

            return v + "        (" +
                `'${act.id.toUpperCase()}', ` +
                `'${act.name}', ` +
                (act.url ? `'${act.url}'` : "NULL") +
                "),\n";
        }, "");

        // remove last comma
        values = values.substring(0,values.length-2) + "\n";

        return MDBStore.query(head + values + tail)
            .map(x => !!x);
    }
}
