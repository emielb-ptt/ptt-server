import { Observable } from "rxjs";

import { PTTStorage } from "../pttstorage";
import MDBStore    from "./mariadb";

export abstract class MDBStorage implements PTTStorage {
    setup(): void {
        MDBStore.setup();
    }

    isSetUp(): Observable<boolean> {
        return MDBStore.isSetUp();
    }
}