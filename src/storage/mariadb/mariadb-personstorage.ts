import { Observable } from "rxjs";

import { Person } from "../../types/person";

import { PersonStorage } from "../personstorage";
import { MDBStorage    } from "./mariadb-storage";
import MDBStore          from "./mariadb";

export class MDBPerson extends MDBStorage implements PersonStorage {
    createOne(person: Person): Observable<number> {
        let prefixString = person.name.prefix ? `'${person.name.prefix}'` : "NULL";
        let activityString = `(SELECT id FROM Activities WHERE name='${person.activity.toString()}')`;

        let queryString =
            "INSERT INTO Persons (\n" +
            "        first_name,\n" +
            "        name_prefix,\n" +
            "        last_name,\n" +
            "        phone,\n" +
            "        activity\n" +
            "    )\n" +
            "    VALUES (\n" +
            `        '${person.name.first}',\n` +
            `        ${prefixString},\n` +
            `        '${person.name.last}',\n` +
            `        '${person.phone}',\n` +
            `        ${activityString}\n` +
            "    );";

        return MDBStore.query(queryString)
            .map(result => result.info.insertId);
    }

    readOne(id: number): Observable<Person> {
        let queryString =
            "SELECT Persons.id,\n" +
            "       Persons.first_name,\n" +
            "       Persons.name_prefix,\n" +
            "       Persons.last_name,\n" +
            "       Persons.phone,\n" +
            "       Activities.name AS activity\n" +
            "    FROM Persons LEFT JOIN Activities\n" +
            "    ON Persons.activity=Activities.id\n" +
            `    WHERE Persons.id=${id};`;

        return MDBStore.query(queryString)
            .map(array => array[0] ? array[0] : null)
            .map(dbo => dbo ? Person.fromDatabaseRecord(dbo) : null);
    }

    readAll(): Observable<Person> {
        let queryString =
            "SELECT Persons.id,\n" +
            "       Persons.first_name,\n" +
            "       Persons.name_prefix,\n" +
            "       Persons.last_name,\n" +
            "       Persons.phone,\n" +
            "       Activities.name AS activity\n" +
            "    FROM Persons LEFT JOIN Activities\n" +
            "    ON Persons.activity=Activities.id;";

        return MDBStore.query(queryString)
            .flatMap((x) => Observable.from(x))
            .map(dbo => dbo ? Person.fromDatabaseRecord(dbo) : null);
    }

    updateOne(id: number, person: Person): Observable<boolean> {
        let prefixString = person.name.prefix ? `'${person.name.prefix}'` : "NULL";
        let activityString = `(SELECT id FROM Activities WHERE name='${person.activity.toString()}')`;

        let queryString =
            "UPDATE Persons\n" +
            `    SET first_name='${person.name.first}',\n` +
            `        name_prefix=${prefixString},\n` +
            `        last_name='${person.name.last}',\n` +
            `        phone='${person.phone}',\n` +
            `        activity=${activityString}\n` +
            `    WHERE id=${id};`;

        /*
         * If rows were affected while UPDATEing, the Person did exist --> return true.
         *
         * If no rows were affected while UPDATEing the table, there are two options:
         * - the id does not belong to a Person -----------------> return false
         * - the given data matches the Person in the database --> return true (idempotency)
         *
         * To figure out which, readOne with the given id and count the number of items found.
         */
        return MDBStore.query(queryString)
            .flatMap(answer => {
                if (answer.info.affectedRows != 0) {// changes --> real Person
                    return Observable.of(true);
                } else {// no changes --> maybe real Person
                    return this.readOne(id)
                        .reduce((acc, curr) => {
                            if (curr) {// id found
                                acc.push(curr);
                            }
                            return acc;
                        }, [])
                        .map(res => res.length > 0);// no items, no Person
                }
            });
    }

    deleteOne(id: number): Observable<boolean> {
        let queryString =
            "DELETE FROM Persons\n" +
            `    WHERE id=${id};`;

        return MDBStore.query(queryString)
            .catch(e => Observable.of(false));
    }
}