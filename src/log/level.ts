export const enum Level {
    debug = 5,
    info = 4,
    warning = 3,
    error = 2,
    panic = 1
}