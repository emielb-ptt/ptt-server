import * as lvl from "./level";
import { Level } from "./level";
import "colors";

export class Logger {
    private level: Level;

    constructor(level: Level|string) {
        this.setLogLevel(level);
    }

    setLogLevel(level: Level|string): void {
        if (typeof level === "string") {
            this.level = (lvl as any).Level[level];
        } else {
            this.level = level;
        }
    }

    getLogLevel(): Level {
        return this.level;
    }

    debug(msg: string): void {
        if (this.level >= Level.debug) {
            console.log("[DEBUG]: ".blue.bold, msg);
        }
    }

    info(msg: string): void {
        if (this.level >= Level.info) {
            console.info("[INFO] : ".green.bold, msg);
        }
    }

    warning(msg: string): void {
        if (this.level >= Level.warning) {
            console.warn("[WARN] : ".yellow.bold, msg);
        }
    }

    error(msg: string): void {
        if (this.level >= Level.error) {
            console.error("[ERROR]: ".red.bold, msg);
        }
    }

    panic(msg: string): void {
        console.error("[PANIC]: FATAL ERROR OCCURRED, TERMINATING".red.bold);
        console.error(`   Error message: ${msg}`.red);
        console.trace();

        //TODO: find a better way of exiting
        process.exit(1);
    }
}

let logger = new Logger(Level.info);
export default logger;
