const gulp = require('gulp');
const ts = require('gulp-typescript');

// Use the project's TypeScript configuration.
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', function () {
    const tsResult = tsProject.src()
        .pipe(tsProject());
    return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('watch', ['scripts'], function () {
    gulp.watch('src/**/*.ts', ['scripts']);
});

gulp.task('default', ['watch']);