# `ptt-server` repo

The Presence Tracking Tool (PTT) is a tool which can be used
to track the occupation of people across different activities.
The tool has been developed for use at the
[JOTI-hunt](http://www.jotihunt.nl), but its use is not limited to that.

Ptt-server is a NodeJS TypeScript Express server, and one of the three
composing parts of the Presence Tracking Tool, which consists of a storage,
a server and a cross-platform mobile app for Android/iOS.

The server is called through its RESTful interface and queries
the storage to retrieve, store or remove data.
See the `swagger` folder for a Swagger documentation on the API.
To serve the documentation on a Swagger UI page, run `npm run swagger`.
By default it will be served at `localhost:8000/docs`.

The server application can be started by running `npm start`
and will be served at `localhost:3000` by default.
The server can provide and manipulate information on the following:

* persons (from v0.1)
* activities (from v0.2)

For details, see the Swagger documentation.

